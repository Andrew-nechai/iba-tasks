const mysql = require("mysql2");
const express = require("express");
var cors = require('cors');

const app = express();

var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200 // For legacy browser support
}

app.use(cors(corsOptions));


const pool = mysql.createPool({
  connectionLimit: 5,
  host: "localhost",
  port: 3306,
  user: "root",
  database: "todotasks",
  password: "1234"
});

const jsonParser = express.json();

app.listen(3001, () => {
  console.log('running on port 3001');
});

app.post('/', jsonParser, (req, res) => {
  try {
    let data = req.body;

    if (req.body.addDeal) {
      let id = data.id;
      let completed;
      if (data.completed) completed = 1
      else completed = 0;
      let text = data.text;

      pool.query(`Insert Into todotasks(id, completed, text) Values(${id}, ${completed}, '${text}')`, (err) => {
        if (err) console.log(err)
        else res.json({success: true});
        console.log("Insert success");
      });
    }

    if (req.body.changeDealStatus) {
      let id = data.id;
      let status = 0;
      if (data.status) status = 1;

      pool.query(`Update todotasks Set completed = ${status} Where id = ${id}`, (err) => {
        if (err) console.log(err);
        else res.json({success: true});
        console.log("Change deal success")
      });
    }

    if (req.body.deleteTask) {
      let id = data.id;
      
      pool.query(`Delete From todotasks Where id=${id}`, (err) => {
        if (err) console.log(err);
        else res.json({success: true});
        console.log("Delete deal success");
      });
    }

    if (data.rewriteData) {
      let taskskeys = data.taskskeys;
      pool.query(`Delete From todotasks Where id In (${taskskeys.join(', ')})`, (err) => {
        if (err) console.log(err);
        else res.json({success: true});
        console.log("Delete deals success");
      });
    }
  }
  catch {
    console.log("ERROR!");
    res.json({success: false});
  }
})

app.post('/deal', jsonParser, (req, res) => {
  try {
    let data = req.body;
    let id = data.id;
    console.log("SUE");
    console.log(data)
    if (req.body.details) {
      let text = data.text;

      pool.query(`Update todotasks Set details = ${text} Where id = ${id}`, (err) => {
        if (err) console.log(err);
        else res.json({success: true});
        console.log("Change details deal success");
      });
    }
    if (req.body.location) {
      let text = data.text;

      pool.query(`Update todotasks Set location = ${text} Where id = ${id}`, (err) => {
        if (err) console.log(err);
        else res.json({success: true});
        console.log("Change location deal success");
      });
    }
    if (req.body.aim) {
      let text = data.text;

      pool.query(`Update todotasks Set text = ${text} Where id = ${id}`, (err) => {
        if (err) console.log(err);
        else res.json({success: true});
        console.log("Change text deal success");
      });
    }
  }
  catch {
    console.log("ERROR!");
    res.json({success: false});
  }
});

app.get('/', jsonParser, (req, res) => {
  pool.query(`SELECT * FROM todotasks`, (err, result) => {
    if (err) console.log(err)
    else res.json(result);
  });
  console.log("Get request for deals")
});