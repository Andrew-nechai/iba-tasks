const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin'); 
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
//const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: ["./src/index.jsx", "./src/index.scss"],
  devtool: "source-map",
  output: {
    filename: "[hash].main.js",
    publicPath: "/",
    path: path.resolve(__dirname, 'dist')
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 3000,
    watchContentBase: true,
    progress: true,
    hot: true,
    historyApiFallback: {
      disableDotRule: true
    }
  },
  
  module: {
    rules: [
      {
        test: /\.jsx$/,
        exclude: /(node_modules)/,
        loader: "babel-loader",
        options: {
          presets:["@babel/preset-env", "@babel/preset-react"],
          sourceMap: true
        }
      },
      {
        test: /\.s[ac]ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {}
          },
          {
            loader: "css-loader",
            options: { importLoaders: 1 } 
          },
          "postcss-loader",
          {
            loader: "sass-loader",
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|svg|jpg)$/,
        loader: "file-loader",
        options: {
          outputPath: 'imgs',
        }
      }
    ]
  },
  plugins: [
    //new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({template: './src/index.html'}),
    
    new webpack.LoaderOptionsPlugin({
      options: { 
        postcss: [
          autoprefixer()
        ]
      }
    }),
    new MiniCssExtractPlugin({
      filename: '[contenthash].style.css'
    }),
  ]
};