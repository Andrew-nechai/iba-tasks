import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import { changeMass, showModal } from './redux/actions.jsx'
import {connect} from 'react-redux'
import Modal from './Modal.jsx'


class AdditInfo extends React.Component {
    constructor(props) {
        super(props);
        this.id = Number(this.props.match.params.id);
        console.log(this.props.getDealsFromBD);
        this.changeDetailsOfDeal = this.changeDetailsOfDeal.bind(this);
        this.changeGoalOfDeal = this.changeGoalOfDeal.bind(this);
        this.changeLocationOfDeal = this.changeLocationOfDeal.bind(this);   
        this.updateInformation = this.updateInformation.bind(this);
        this.setState = this.setState.bind(this);
        this.state = {
            whatNeedToChange: ""
        };
    }

    updateInformation() {
        console.log("AddInfo UpdateInformation: ", this.props.mass);
        let get_main_info = this.props.mass.find((item) => {
            return item.id == this.id
        });

        this.aim = get_main_info.text || "-";
        this.details = get_main_info.details || "-";
        this.location = get_main_info.location || "-";
    }

    changeDetailsOfDeal() {
        this.setState({
            whatNeedToChange: "details"
        });
        this.props.showModal(true);
    }

    changeGoalOfDeal() {
        this.setState({
            whatNeedToChange: "aim"
        });
        this.props.showModal(true);
    }

    changeLocationOfDeal() {
        this.setState({
            whatNeedToChange: "location"
        });
        this.props.showModal(true);
    }

    render() {
        this.updateInformation();
        console.log("RENDER!")
        return (
        <div className="additional-info">
            <div className="additional-info__wrapper">
                <Link to="/">
                <div className="additional-info__back-button">
                    <div className="back-button__arrow"></div>
                </div>
                </Link>
                <div className="additional-info__data todolist__content">
                <table cols="2" cellSpacing="0">
                    <tbody>
                    <tr>
                    <td>{"Цель: "}</td>
                    <td>{this.aim}</td>
                    </tr>
                    <tr>
                    <td>{"Детали: "}</td>
                    <td>{this.details}</td>
                    </tr>
                    <tr>
                    <td>{"Место выполнения: "}</td>
                    <td>{this.location}</td>
                    </tr>
                    </tbody>
                </table>
                </div>
                <div className="additional-info__toolbuttons">
                <div className="buttons-block__button change-title-task" onClick={this.changeGoalOfDeal}>{"Изменить цель"}</div>
                <div className="buttons-block__button add-details" onClick={this.changeDetailsOfDeal}>{"Детали"}</div>
                <div className="buttons-block__button set-place" onClick={this.changeLocationOfDeal}>{"Место выполнения"}</div>
                </div>
            </div>
            <Modal type={this.state.whatNeedToChange} id={this.id} parentSetState={this.setState}/>
        </div>        
        );
    }
}

function mapStateToProps(state) {
    return {
        mass: state.mass
    }
  }
  
  function mapDispatchProps(dispatch) {
    return {
      changeMass: (mass) => dispatch(changeMass(mass)),
      showModal: (bool) => dispatch(showModal(bool))
    } 
  }

export default connect(mapStateToProps, mapDispatchProps)(AdditInfo);