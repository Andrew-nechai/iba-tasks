import React from 'react';
import ToolButtons from './ToolButtons.jsx'

class ButtonsBlock extends React.Component{
    constructor(props) {
        super(props);
        this.clickEvent = this.clickEvent.bind(this);
        this.deleteTasks = this.deleteTasks.bind(this);

        this.resetSelectTasksStyle = this.props.resetSelectTasksStyle;
        this.resetToolButtonsStyle = this.props.resetToolButtonsStyle;
        this.changeParentTasksState = this.props.changeParentTasksState;
        this.changeParentButtonsState = this.props.changeParentButtonsState;
        this.emptyTasksMas = this.props.emptyTasksMas;
        this.changeCountUncheck = this.props.changeCountUncheck;
        this.deleteDeals = this.props.deleteDeals;
    }

    clickEvent(e) {
        e.stopPropagation();
        if (!this.emptyTasksMas()) {
            alert("Список дел пуст!")
            return
        }
        let but_mass = this.props.buttonsmass;
        let errors = "";

        this.resetSelectTasksStyle();

        but_mass = but_mass.map((button) => {
            if (button.name.toUpperCase() == e.target.value.toUpperCase()){
                if (!button.klicked) {
                    errors = this.selectTasks(button.name);
                } 
                if (errors == "Error") {
                    return button
                }
                button.klicked = !button.klicked;
            } 
            else {
                button.klicked = false;
            }
            return button
        });

        console.log("Buts after method: ", but_mass)
        
        this.changeParentButtonsState(but_mass);
    }

    selectTasks(type = "all") {
        let tasks = this.props.parentstate.mass.slice(0);
        if (type == "all") {
            this.changeParentTasksState(tasks.map(task => {
                task.selected = true;
                return task;
            }));
        }
        let count = 0;
        if (type == "active") {
            tasks = tasks.map(task => {
                if (!task.completed){
                    task.selected = true;
                    count++;
                }                
                return task;
            });
            if (!count) {
                alert("Нет активных дел!");
                return "Error";
            }
            
            this.changeParentTasksState(tasks); 
        }
        if (type == "complete") {
            tasks = tasks.map(task => {
                if (task.completed){
                    task.selected = true;
                    count++;
                }                
                return task;
            });
            if (!count) {
                alert("Нет завершенных дел!");
                return "Error";
            }
            console.log(tasks)
            this.changeParentTasksState(tasks);
        }
    }

    deleteTasks(tasks) {
        if (!this.emptyTasksMas()) {
            alert("Список дел пуст!");
            return;
        }   
        if (this.props.isSelectedItems()) {
            let res = window.confirm("Удалить выбранные дела?");
            if (res) {
                //let mas = this.props.parentstate.mass.filter(element => element.selected !== true);
                let mas = this.props.parentstate.mass;
                let deletedItemsIds = [];

                mas.forEach((item) => {
                    if (item.selected) {
                        deletedItemsIds.push(item.id)
                    }
                });

                let count = mas.length-deletedItemsIds.length;
                // mas.forEach(element => {
                //     if (!element.completed) {
                //         count++;
                //     }
                // });

                console.log(this.props.parentstate);
                this.changeCountUncheck(count);
                this.deleteDeals(deletedItemsIds);
                this.resetToolButtonsStyle();
            }   
        }
        else {
            alert("Выберите элементы для удаления!");
        }
    }

    render() {
        return (
            <div className="buttons-block">
                {this.props.buttonsmass.map(element => {
                    let nameClass = "buttons-block__button";
                    if (element.klicked) {
                        nameClass += " buttons-block__button_fill";
                    }
                    return (<ToolButtons key = {element.name} someid = {element.name} className={nameClass} value={element.name} onClick={this.clickEvent}/>);
                })
                }
                <input className="buttons-block__button buttons-block__button_delete buttons-block__button_fill_r" type="button" value="CLEAR" onClick = {this.deleteTasks}/>
            </div>
        );
    }
}

export default ButtonsBlock