import React from 'react';

function ToolButtons(props) {
    // let result = [];
    // props.buttonsmas.forEach(element => {
    //     let nameClass = "buttons-block__button";
    //     if (element.klicked) {
    //         nameClass += " buttons-block__button_fill";
    //     }
    //     result.push({className: nameClass, value: element.name.toUpperCase()});
    // });
    return (
        <input type="button" className={props.className} value={props.value.toUpperCase()} onClick={props.onClick}/>
    );
}

export default ToolButtons