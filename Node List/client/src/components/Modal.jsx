import React from 'react';
import CloseButton from './img/close-modal.svg'
import {connect} from 'react-redux'
import { CSSTransition } from 'react-transition-group'
import {showModal} from './redux/actions.jsx'
import { changeMass } from './redux/actions.jsx'

class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.modalAnimationName = "";
        this.wrapperModalAnimationName = "";
        this.closeModal = this.closeModal.bind(this);
        this.changeAdditInfo = this.changeAdditInfo.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.updateDealDataInBd = this.updateDealDataInBd.bind(this);
        this.getDealsFromBD = this.getDealsFromBD.bind(this);
        //this.saveInLocalStorage = this.saveInLocalStorage.bind(this);
        this.state = {value: ''};
        //console.log(this.props.getDealsFromBD())
    }   

    // saveInLocalStorage(mass) {
    //     localStorage.setItem('root', JSON.stringify(mass));
    // }

    closeModal() {
        this.props.showModal(false);
    }

    changeAdditInfo(e) {
        e.stopPropagation();
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    updateDealDataInBd(resolves, reject, idd, textt, type) {
        let objectRes = {
            'id': idd,
            'text': textt
        }

        objectRes[type] = "true";

        objectRes[type] = true;
        console.log("updateDealDataInBD object: ", objectRes);

        let response = fetch('http://localhost:3001/deal', {
            method: 'POST', 
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *client
            body: JSON.stringify(objectRes)
            }).then((res) => {
                return res.json();
            }).then((res) => {
            if (res) {
                console.log("Update Successfully");
                this.getDealsFromBD();
                resolves(res);
            }
            else {
                reject();
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        let text = this.state.value;
        let id = this.props.id;
        // let mass = this.props.mass.map((item, index) => {
        //     if (item.id == this.props.id) {
        //         if (this.props.type == "aim") {
        //             item.text = text;
        //         }
        //         else {
        //             if (this.props.type == "details") {
        //                 item.details = text;
        //             }
        //             else {
        //                 item.location = text;
        //             }
        //         }
        //     }
        //     return item;
        // });
        new Promise((resolves, reject) => {
            this.updateDealDataInBd(resolves, reject, id, text, this.props.type);
        }).then((res) => {
            if (res.success) {
                //this.saveInLocalStorage(mass);
                // this.props.parentSetState({
                //     whatNeedToChange: ""
                // });
                //this.closeModal();
            } else {
                console.log("Error");
            }
        });
    }

    getDealsFromBD() {
        let request = fetch("http://localhost:3001", {
          method: 'GET'
        }).then((res) => {
          return res.json();
        }).then((res) => {
          //this.props.changeMass(changeMass(res.reverse()));
        });
      }

    render() {
        if (this.props.isShowModal) {
            return (
                <CSSTransition
                    in={true}
                    classNames="bgModal"
                    timeout={1000}
                >
                <div className="change_deal_block">
                    <div className="change_deal_block__wrapper">
                        <CSSTransition
                            in={this.props.isShowModal}
                            classNames="modal"
                            timeout={1000}
                        >
                        <div className="change_deal">
                            <div className="change_deal__title">
                                <div className="change_deal__title_text">{"Введите данные"}</div>
                                <div className="change_deal__exit" onClick={(e) => {this.closeModal()}}><img src={CloseButton} alt="X"/></div>
                            </div> 
                            
                            <form onSubmit={this.handleSubmit}>
                                <div className="change_deal__input"><input type="text" value={this.state.value} onChange={this.handleChange}  placeholder="Введите значение"/></div>
                                <div className="change_deal__button"><button type="submit" onClick={this.changeAdditInfo}>{"Submit"}</button></div>
                            </form>
                        </div>
                        </CSSTransition>
                    </div>
                </div>
                </CSSTransition>
            );
        } 
        else {
            return (<span></span>);
        }
    }
}

function mapStateToProps(state) {
    return {
        isShowModal: state.showModal,
        mass: state.mass
    }   
}

function mapDispatchProps(dispatch) {
    return {
        showModal: bool => dispatch(showModal(bool)),
        changeMass: (mass) => dispatch(changeMass(mass))
    }
}

export default connect(mapStateToProps, mapDispatchProps)(Modal);