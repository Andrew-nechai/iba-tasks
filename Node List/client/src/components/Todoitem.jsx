import React from 'react';
import checked_img from './img/checked.svg';
import unchecked_img from './img/unchecked.svg';
import more_img from './img/more.svg';
import delete_img from './img/delete.svg';
import {Link} from "react-router-dom";


function Todoitem(props) {
    let state = "";
    let img_src;
    let add_class = "";
    let select_class = "";


    if (props.completed) {
        state = "checked";
        img_src = checked_img;
        add_class = "text-throught";
    } else {
        state = "unchecked";
        img_src = unchecked_img;
    }

    if (props.selected) {
        select_class = "task task-selected";
    }
    else {
        select_class = "task";
    }
    
    return (
        <div className={select_class} onClick={
            (e) => {
                e.stopPropagation();
                if (props.isButtonKlicked()) {
                    props.resetSelectTasksStyle();
                }
                props.resetToolButtonsStyle();
                props.onSelect(props.id_el);
            }
        }>
            <div className={"checkbox " + state} onClick={(e) => {
                    e.stopPropagation();
                    if (props.isButtonKlicked()) {
                        props.resetSelectTasksStyle();
                    }
                    props.onChange(props.id_el);
                }}>
                <img src={img_src} alt="Yes"/>
            </div>
            <div className={"task__text " + add_class}>{props.text}</div>
            <div className="todoitem__addinfo" onClick={(e) => {
                e.stopPropagation();
                
            }}><Link to={`/deal/${props.id_el}`}><img src={more_img}></img></Link></div> 
            <div className="basket" onClick={(e) => {
                e.stopPropagation();
                if (props.isButtonKlicked()) {
                        props.resetSelectTasksStyle();
                    }
                props.deleteTask(props.id_el, props.checked);
            }}><img src={delete_img} alt="X"/></div>
        </div> 
    )
}

export default Todoitem