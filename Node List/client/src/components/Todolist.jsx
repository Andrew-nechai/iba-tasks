import React from 'react';
import Todoitem from './Todoitem.jsx'
// import ButtonsBlock from './ButtonsBlock.jsx'

function Todolist(props){
    return (
        <div className="todolist__content" key = "2">
            {   props.massiv.map((task, i) => {
                return (
                    <Todoitem 
                        resetToolButtonsStyle = {props.resetToolButtonsStyle}
                        resetSelectTasksStyle = {props.resetSelectTasksStyle}
                        isButtonKlicked = {props.isButtonKlicked}
                        text = {task.text} completed = {task.completed} selected = {task.selected} key = {task.id} onChange = {props.onChange} deleteTask = {props.deleteTask} onSelect = {props.onSelect} checked = {task.completed} id_el = {task.id}
                    />
                )    
            })}         
        </div>
    );
}

export default Todolist;