import React from 'react';
import TitleTodo from './TitleTodo.jsx'
import InputTodo from './InputTodo.jsx'
import Todolist from './Todolist.jsx'
// import Modal from './Modal.jsx'
import AdditInfo from './AdditInfo.jsx'
import ButtonsBlock from './ButtonsBlock.jsx'
import {Route, Switch} from 'react-router-dom'
import {changeMass, changeButMass, removeSelectItems} from './redux/actions.jsx'
import {connect} from 'react-redux'
import Axios from 'axios'


class App extends React.Component {
  constructor(props) {
      super(props);
      this.title = "TODOLIST";
      this.placeholdertodo = "Добавить задачу";
      this.count_check_tasks = this.props.mass.filter(task => task.completed == false).length;
      this.parent = this.props.parent;
      this.onChange = this.onChange.bind(this);
      this.handleKeyPress = this.handleKeyPress.bind(this);
      this.deleteTask = this.deleteTask.bind(this);
      this.getRandomId = this.getRandomId.bind(this);
      this.changeCountTasks = this.changeCountTasks.bind(this);
      this.selectTask = this.selectTask.bind(this);
      this.changeTasksState = this.changeTasksState.bind(this);
      this.changeButtonsState = this.changeButtonsState.bind(this);
      this.emptyTasksMas = this.emptyTasksMas.bind(this);
      this.isButtonKlicked = this.isButtonKlicked.bind(this);
      this.isSelectedItems = this.isSelectedItems.bind(this);
      this.changeCountUncheck = this.changeCountUncheck.bind(this);
//      this.saveInLocalStorage = this.saveInLocalStorage.bind(this);
      this.resetToolButtonsStyle = this.resetToolButtonsStyle.bind(this);
      this.resetSelectTasksStyle = this.resetSelectTasksStyle.bind(this);
      this.addDealToBD = this.addDealToBD.bind(this);
      this.deleteSelectedDeals = this.deleteSelectedDeals.bind(this);
      this.getDealsFromBD = this.getDealsFromBD.bind(this);
      this.rewriteBdInfo = this.rewriteBdInfo.bind(this);

      this.getDealsFromBD();
      // this.props = {
      //   mass: [],
      //   buttons: [
      //     {name: "all", klicked: false},
      //     {name: "active", klicked: false},
      //     {name: "complete", klicked: false}
      //   ]
      // };
  }

  componentDidMount(){
    // let newmas = JSON.parse(localStorage.getItem(this.parent)) || [];
    // let object = changeMass(newmas);
    // this.props.changeMass(object);
    // this.count_check_tasks = this.props.mass.filter(task => task.completed == false).length;
  } 
  
  // saveInLocalStorage(mass) {
  //   localStorage.setItem(this.parent, JSON.stringify(mass));
  // }

  getRandomId() {
    let mas = this.props.mass.map(item => item.id); 
    let otv;
    while (true) {
      otv = Math.floor(Math.random() * (30000 - 1) + 1);
      if (!mas.includes(otv)) {
        break;
      }
    }
    return otv;
  }

  handleKeyPress(event) {
    let mas = this.props.mass.slice(0);
    if (event.currentTarget.value === "") {
      return;
    }
    if (event.keyCode == 13) { 
        this.count_check_tasks++;  
        let generated_id = this.getRandomId(); 
        let task = {id: generated_id, completed: false, text: event.currentTarget.value};
        mas.unshift(task);
        event.currentTarget.value = "";
        
        new Promise((resolve, reject) => {
          this.addDealToBD(resolve, reject, task);
        }).then((r, e) => {
          if (!e) {
            this.resetToolButtonsStyle();
            //this.saveInLocalStorage(mas);
            this.resetSelectTasksStyle();
            this.props.changeMass(changeMass(mas));
          }
        });
    }
  }

  changeCountTasks(task) {
    if (task.completed) {
      this.count_check_tasks--;
    } 
    else {
      this.count_check_tasks++;
    }
  }

  onChange(id) {
    let mas = this.props.mass.slice(0);
    let isChecked = false;

    mas.map(task => {
      if (task.id == id) {
        task.completed = !task.completed;
        isChecked = task.completed;
        if (task.completed) {
          this.count_check_tasks--;
        } 
        else {
          this.count_check_tasks++;
        }
      }
    });
  
  //запрос на изменение чекбокса
  let request = fetch(`http://localhost:3001`, {
    method: 'POST', 
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify({
      changeDealStatus: true,
      id: id,
      status: isChecked
    })
  }).then((res) => {
    return res.json();
  }).then((res) => {
    if (res.success) {
      //setTimeout(() => {
        console.log("Changed");
        this.getDealsFromBD();
        this.resetToolButtonsStyle();
      //}, 1000);
    }
  });
}

  changeTasksState(){
    let newmass = this.props.mass.slice(0);

    let count = 0;
    
    newmass.forEach(element => {
        if (!element.completed) {
            count++;
        }
    });

    this.changeCountUncheck(count);
    this.props.removeSelectItems(removeSelectItems(newmass)); 
  }

  changeButtonsState(newbuttons){
    this.props.changeButMass(changeButMass(newbuttons));
  }

  deleteTask(id, check) {
    let mas = this.props.mass.filter((task) => task.id !== id);
    
    if (!check) {
      this.count_check_tasks--;
    }
    
    //запрос на удаление
    let request = fetch(`http://localhost:3001`, {
      method: 'POST', 
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer',
      body: JSON.stringify({
        deleteTask: true,
        id: id
      })
    }).then((res) => {
      return res.json();
    }).then((res) => {
      console.log("Deleted");
      // setTimeout(() => {
        this.getDealsFromBD();
        this.resetToolButtonsStyle();
      // }, 1000);
    });
  }

  selectTask(id) {
    let mas = this.props.mass.slice(0);
    this.props.mass.map(task => { 
      if (task.id == id) {
        task.selected = !task.selected;
      }
    });
    this.props.changeMass(changeMass(mas));
  }

  resetToolButtonsStyle() {
    let buttons_mas = this.props.buttons.map(button => {button.klicked = false; return button});
    this.props.changeButMass(changeButMass(buttons_mas));
  }

  resetSelectTasksStyle() {
    let tasks_mas = this.props.mass;
    tasks_mas.map(task => {
        task.selected = false;
    });
    
    this.props.changeMass(changeMass(tasks_mas));
  }    

  emptyTasksMas() {
    if (this.props.mass.length) {
      return true
    }
    return false;
  } 

  isButtonKlicked() {
    let otv = false;
    this.props.buttons.forEach(element => {
      if (element.klicked) {
        otv = true;
      }
    });
    return otv;
  }


  isSelectedItems() {
    let otv = false;
    this.props.mass.map((element) => {
      if (element.selected) {
        otv = true;
      }
    });
    return otv;
  }

  changeCountUncheck(count) {
    this.count_check_tasks = count;
  }

  deleteSelectedDeals(tasks) {
    this.rewriteBdInfo(tasks)
    // this.props.changeMass(changeMass(tasks));
    //this.saveInLocalStorage(tasks);
  }

  rewriteBdInfo(tasks) {
    let response = fetch('http://localhost:3001', {
      method: 'POST', 
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *client
      body: JSON.stringify({taskskeys: tasks, rewriteData: true}),

    }).then((res) => {
      return res.json();
    }).then((res) => {
      if (res) {
        console.log("Rewrite success!");
        this.getDealsFromBD();
      }
    });
  }

  addDealToBD(resolves, reject, task) {
    let response = fetch('http://localhost:3001', {
      method: 'POST', 
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *client
      body: JSON.stringify({... task, addDeal: true}),

    }).then((res) => {
      return res.json();
    }).then((res) => {
      if (res) {
        console.log("Add succesfully");
        resolves();
      }
      else {
        reject();
      }
    });
  } 
  
  // let response = await fetch('http://localhost:3001', {
  //   method: 'POST',
  // });
  // let response2 = await response.json();
  
  // console.log(response2);

  getDealsFromBD() {
    let request = fetch("http://localhost:3001", {
      method: 'GET'
    }).then((res) => {
      return res.json();
    }).then((res) => {
      this.props.changeMass(changeMass(res.reverse()));
    });
  }

  render() {
    return (
        <div>
          <Route path="/" exact render={() => 
            <section className="todolist">
                <TitleTodo text = {this.title}/>
                <InputTodo keypress = {this.handleKeyPress}/>
                <Todolist massiv = {this.props.mass} 
                          onChange = {this.onChange} 
                          deleteTask = {this.deleteTask} 
                          onSelect = {this.selectTask}
                          resetToolButtonsStyle = {this.resetToolButtonsStyle}   
                          resetSelectTasksStyle = {this.resetSelectTasksStyle}
                          isButtonKlicked = {this.isButtonKlicked}  
                />
                <div className="todolist__toolbar">
                    <div className="count-uncheck-deals">
                        { 
                          "Количество невыполненных дел: " + this.count_check_tasks
                        }
                    </div>
                    <ButtonsBlock changeParentTasksState = {this.changeTasksState} 
                                  deleteDeals = {this.deleteSelectedDeals}
                                  parentstate = {this.props} 
                                  buttonsmass = {this.props.buttons}
                                  changeParentButtonsState = {this.changeButtonsState} 
                                  resetToolButtonsStyle = {this.resetToolButtonsStyle}
                                  resetSelectTasksStyle = {this.resetSelectTasksStyle}
                                  isSelectedItems = {this.isSelectedItems}
                                  emptyTasksMas = {this.emptyTasksMas}
                                  changeCountUncheck = {this.changeCountUncheck}
                                  />
                </div>
              </section>} />
          <Route exact path="/deal/:id" component={AdditInfo}/>
      </div>
    );
    //saveInLocalStorage = {this.saveInLocalStorage}
  }
}

function mapStateToProps(state) {
  return {
      mass: state.mass,
      buttons: state.buttons
  }
}

function mapDispatchProps(dispatch) {
  return {
    changeMass: (obj) => dispatch(obj),
    changeButMass: (obj) => dispatch(obj),
    removeSelectItems: (obj) => dispatch(obj)
  } 
}

export default connect(mapStateToProps, mapDispatchProps)(App);