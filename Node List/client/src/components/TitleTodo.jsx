import React from 'react';

class TitleTodo extends React.Component {
    render() {
        return (
            <div className="todolist__title">
                <a href="#">
                    {this.props.text}
                </a>
            </div>
        );
    }
}

export default TitleTodo