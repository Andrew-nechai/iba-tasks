// import { ADDDEAL, DELETEDEAL, CHECKBOXCHANGE, SELECTDEALS, RESETBUTTONS, RESETDEALSSTYLE } from "./types";
import { CHANGEMASS, CHANGEBUTMASS, REMOVESELECTITEMS, SHOWMODAL } from "./types";


async function initialState() {
    new Promise((resolve, reject) => {
        let request = fetch("http://localhost:3001", {
            method: 'GET'
        }).then((res) => {
            return res.json();
        }).then((res) => {
            if (res.success) {
                resolve();
            }    
        });
    }).then((res) => {
        let deals = res;
        let initialState = {
            mass: deals,
            buttons: [
                {name: "all", klicked: false},
                {name: "active", klicked: false},
                {name: "complete", klicked: false}
            ],
            showModal: false
        };

        return initialState;
    });
}


export default async function rootReduser(state = initialState(), action) {
    switch (action.type) {
        case CHANGEMASS:    
            return {
                ...state, mass: action.mass
            }

        case REMOVESELECTITEMS: 
            return {
                ...state, mass: action.mass
            }

        case CHANGEBUTMASS:
            return {
                ...state, buttons: action.buttons
            }

        case SHOWMODAL:
            return {
                ...state, showModal: action.showModal
            }
        // case ADDDEAL:
        //     return {
        //         ...state, mass: action.mass
        //     };
        //     break;
        
        // case DELETEDEAL: 
        //     return {
        //         ...state, mass: action.mass
        //     };
        //     break;
        // case CHECKBOXCHANGE: 
        //     return {
        //         ...state, mass: action.mass
        //     };
        //     break;

        // case SELECTDEALS: 
        //     console.log("action.mass имеет правильный массив, но почему-то в state свойство mass не заменяется");
        //     console.log("MASS1", action.mass);
        //     return {
        //         ...state, mass: action.mass
        //     };
        //     break;
        
        // case RESETBUTTONS:
        //     return {
        //         ...state, buttons: action.buttons
        //     }
        
        // case RESETDEALSSTYLE:
        //     return {
        //         ...state, mass: action.mass
        //     }
        
        default:
            return state;
            break;
    }
};