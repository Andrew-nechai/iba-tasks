import { CHANGEMASS, CHANGEBUTMASS, REMOVESELECTITEMS, SHOWMODAL } from "./types";
         
export function changeMass(mass) {
    return {
        type: CHANGEMASS,
        mass: mass  
    }
}

export function changeButMass(mass) {
    return {
        type: CHANGEBUTMASS,
        buttons: mass  
    }
}

export function removeSelectItems(mass) {
    return {
        type: REMOVESELECTITEMS,
        mass: mass
    }
}

export function showModal(bool) {
    return {
        type: SHOWMODAL,
        showModal: bool
    }
}